---
geometry: margin=0.66in
fontsize: 9pt
header-includes:
    - \setlength{\parskip}{0.75in}
---

# MI-W20 2017/18 Self-test sheet na teorii

Vysvětlete termín prosumers.

Pokud budete mít vlastní dílo, kdy použijete licenci Creative Commons: Share Alike?

Vysvětlete spojitost mezi Atom Syndicate formátem a AtomPub protokolem.

K čemu využijete service, workspace a collection u Atomu.

Co je to JSONP? K čemu se používá?

Co je to AJAX Crawling?

Co je to Triadic Closure u SNA?

Jaké restrikce přináší publikování produktu pod licencí Creative Commons: Share Alike?

Co je to "dot com bubble"?

Uveďte alespoň 4 příklady Web 1.0 aplikací a k nim 4 protipříklady Web 2.0 aplikací.

Uveďte 3 vrstvy jazyků pro sémantiku používaných na webu.

Jaký je rozdíl mezi mikrodaty a mikroformáty?

Co je to "Same origin policy"? Čemu zamezuje?

Jak jsou reprezentovány stavy v aplikacích Web 2.0?

K čemu se používá OpenGraph?

Jaké jsou typické úlohy fiktivních uživatelů jménem Alice, Bob, Eve a Malory? Co každý z nich představuje?

Uveďte alespoň 3 hlavní znaky Web 2.0.

Jaké jsou 3 hlavní pilíře RDF?

Uveďte rozdíl mezi "Mashup Service" a "Application Mashup".

Jaká je spojitost mezi OAuth a SaaS?

Co je to "crowdsourcing"?

Vysvětlete rozdíl mezi synchronním a asynchronním I/O.

> Při neblokujícím I/O spravuje připojování OS, ne web app.

Popište kroky navázání komunikace pomocí WebSocketu.

Co je to CAP theorem?

Co je to XSS? Jak funguje?

Uveďte 3 různé možnosti jak streamovat data na webu.

Co jsou to "typed literals"? Kde se využívají?

Kdy je vhodné použít JSON a kdy XML?

Jaký je rozdíl mezi webovou službou a webovým API?

Co jsou to Linked-Data?

Co je URI reference?

Co je to DOM?

Uveďte 3 typy mashupu.

Co je to RDFa, k čemu je dobrý a uveďte příklad.

Co je to CORS?

Jaký je rozdíl mezi Copyright a Creative Commons?

Jakou Web 2.0 vlastnost bychom přiřadili Wikipedii?

Co definuje specifikace AtomPub?

Jsou HTTP metodu bychom použili pro zhasnutí světla v místnosti?

Jaký je rozdíl mezi XSS a CSRF?

Rozhodněte o správnosti tvrzení a zdůvodněte vaši odpověď: "Jazyk RDF definuje třídy a vztahy mezi nimi."

Uveďte jiné technologie pro streamování dat na webu, kromě WebSocket.

Vyjmenujte metody pro real-time web.

Co je to Slug?

Jak je reprezentován stav aplikace v HATEOAS?

K čemu slouží HTTP metoda PATCH?

Co je to Conditional PUT? Kdy a kde se používá?

Co je predicate v RDF?

Jaký je rozdíl mezi pollingem a long-pollingem?

Vysvětlete co znamená COMET a jakými dvěma způsoby je možné COMET realizovat.

Rozdíl mezi technikami pro streaming SSE a WebSocket.

K čemu slouží autorizační endpoint a tzv. token endpoint u OAuth2? Jaký je mezi nimi rozdíl?

Jaký je rozdíl mezi proprietárním XML, AtomFeed a RDF?

Pomocí čeho lze v RDF vytvořit entitu bez globálního identifikátoru?

Co je to "closure" v Javascriptu?

Vysvětlete betweenness metriku.

Co je to Server-Sent Events? Popište na straně serveru i klienta.

Jaký je rozdíl OpenID a OAuth protokoly?

Jak se nazývá hlavní bezpečnostní omezení AJAX aplikace při vývoji mashup aplikace na straně klienta a jak je možné toto omezení "obejít"?

Vysvětlete termín "Multitenancy".

Vysvětlete souvislosti mezi mikroslužbou, kontejnerizací a multi-tenancy.

Uveďte tři technologie kernelu linuxu, které se využívají pro kontejnerizaci.

Jaký je rozdíl mezi WebSocket s XHR?

K čemu je TURN pro WebRTC komunikaci?

Co je to JSON Web Token a k čemu se používá? Uveďte příklad toho, co se v hlavičce objeví.

Co jsou to omezení REST a uveďte příklad jednoho omezení.

Jaký je rozdíl mezi mikroslužbou a REST službou?

Jaký je rozdíl mezi autorizací a autentizací?

Uveďte rozdíl mezi strong a weak ETagem.

Vysvětlete rozdíl mezi URI a URL.

Vysvětlete SWARM v Dockeru.

Co je to OAuth2 a v čem spočívá přínos OAuth2 pro Web 2.0?

Vysvětlete rozdíl mezi IaaS a PaaS.

K čemu slouží STUN?

Co jsou to data volumes v technologii Docker?

Jak autentizovat uživatele použitím JSONP?

> No moc dobře ne ^[https://www.google.com/search?client=safari&rls=en&q=jsonp+authentication&ie=UTF-8&oe=UTF-8] 

Popište jak funguje CORS.

Co definuje Atom Syndication Format?

K čemu v HTTP Digest slouží hodnota "nonce"?

Co znamená, že Microformát neškáluje?

K čemu slouží iniciativa schema.org?

Co znamená Representational State Transfer?

Co je to uniform interface, uveďte jeden příklad a jaké operace zahrnuje?


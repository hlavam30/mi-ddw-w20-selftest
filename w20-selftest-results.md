---
geometry: margin=0.66in
fontsize: 9pt
header-includes:
    - \setlength{\parskip}{0.1in}
---

# MI-DDW 2017/18 Self-test sheet na teorii — výsledky

*Self-test sheet pro MI-W20, obsahující otázky a odpovědi z fit-wiki zkoušek z LS 2016/2017. Některé odpovědi jsou přímo z fit-wiki, některé jsou opravené a některé jsou doplněné vlastní (snad oceníte). Doporučený postup: Vytisknout si self-sheet a udělat si test "nanečisto" a napsané výsledky ověřit oproti řešení níže.*

Vysvětlete termín prosumers.

> Producer, který je zároveň consumer. (např. uživatelé Wikipedia, Facebook)

Pokud budete mít vlastní dílo, kdy použijete licenci Creative Commons: Share Alike?

> Pokud chceme dílo šířit pod licencí, která dovoluje šíření, adaptaci, ale chcete, abyste byli zmínění jako autoři a aby další dílo bylo šířeno pod stejnou licencí.

Vysvětlete spojitost mezi Atom Syndication formátem a AtomPub protokolem.

> AtomPub je protokol pro publikování a editování webových zdrojů ve formátu Atom Syndication Format.[^ https://bitworking.org/projects/atom/rfc5023.html]

K čemu využijete service, workspace a collection u Atomu.

> Service je služba, která je dokumentem popisována (v dokumentu uvedena většinou jen jednou) a workspace je část service obsahující collections. [^ https://books.google.cz/books?id=XUaErakHsoAC&pg=PA277&lpg=PA277&dq=atom+workspace+collection&source=bl&ots=5ldozjmMmA&sig=HA5gjMjbbFov7VdE93dXJ5FPp_0&hl=en&sa=X&ved=0ahUKEwit3vbcybfbAhUIjiwKHd8tDqwQ6AEIRzAD#v=onepage&q=atom%20workspace%20collection&f=false]

Co je to JSONP? K čemu se používá?

> Způsob, jak požádat o data a následně tato data převzít JS skriptem. Kdyby bylo o data požádáno čistě jako JSON, data by se načetla, přenesla do paměti, ale nebyla by k ničemu přiřazena (žádná proměnná v JS, nic)

Co je to AJAX Crawling?

> Crawling s respektováním AJAXových (lazy load) načítání stránky (spouští javascriptová volání po načtení stránky, aby ji mohl naindexovat). Dělá např. Google.

Jaké restrikce přináší publikování produktu pod licencí Creative Commons: Share Alike?

> BY: Uvedení autora, SA: sdílení produktu pod stejnou (či přísnější) licencí CC.

Co je to "dot com bubble"?

> Technologický trend rozšiřování internetu do běžného života v letech 1997-2001. Dot com bubble jelikož vznikalo spousta e-firem a firem.com názvů.

Uveďte alespoň 4 příklady Web 1.0 aplikací a k nim 4 protipříklady Web 2.0 aplikací.

> - Britanica Online -> Wikipedia
> - Personal websites -> Facebook
> - Personal blogs -> Twitter
> - directories -> search engines

Uveďte 3 vrstvy jazyků pro sémantiku používaných na webu.

> TODO:

Jaký je rozdíl mezi mikrodaty a mikroformáty?

> Podobná idea, ale mikroformáty nejsou škálovatelné, mají nadefinován svůj pevně daný slovník a používá k metadatům atributů `class`, který koliduje s CSS.

Co je to "Same origin policy"? Čemu zamezuje?

> Same origin policy povoluje omezit domény, ze kterých může být spušten nějaký HTTP request (např. POST). Zamezuje CSRF útokům.

Jak jsou reprezentovány stavy v aplikacích Web 2.0?

> *Otázka je obecně nesmysl (W2.0 aplikace mohou fungovat úplně totožně jako W1.0, co se týče stavů aplikace a stále to mohou být W2.0 aplikace), ale stejně jako spousta dalších otázek z MI-W20 nad tím musíte mávnout rukou a zapamatovat si, že* pokud je použit JavaScript, tak je stav většinou reprezentován hashbangem v URL (http://eh.wat/kek#!inbox)

K čemu se používá OpenGraph?

> K definici metadat dané stránky (většinou pro zobrazení rich obsahu u odkazů na Facebooku)

Jaké jsou typické úlohy fiktivních uživatelů jménem Alice, Bob, Eve a Malory? Co každý z nich představuje?

> Alice a Bob jsou subjekty, které chtějí spolu komunikovat, Eve je pasivní útočník a Malory aktivní.

Uveďte alespoň 3 hlavní znaky Web 2.0.

> - Prosuming (ne jen consuming)
> - Dynamické HTML (ne jen statické HTML)
> - Mashups, SaaS (vzájemné propojení služeb)

Co je RDF triple? Vyjmenujte.

> subject, predicate, object

Uveďte rozdíl mezi "Mashup Service" a "Application Mashup".

> Mashup service je samo o sobě služba, která navíc využívá dalších 3rd party služeb, je tedy read-write, kde Application Mashup je spíše jen aplikace využívající 3rd party služby, ale samo o sobě není aplikací a je read-only (nějaký agregátor, sjednocující UI, apod.)

Jaká je spojitost mezi OAuth a SaaS?

> OAuth přímo podporuje rozvoj SaaS, můžeme tak nabízet software jako službu a zároveň zajistit bezpečné 3rd party přihlášení pomocí OAuth

Co je to "crowdsourcing"?

> Společná snaha k vyřešení nějakého problému (reCAPTCHA, kickstarter, ...)

Vysvětlete rozdíl mezi synchronním a asynchronním I/O.

> Při neblokujícím I/O spravuje připojování OS, ne web app. Pro každé připojení neblokující I/O vytváří samostatné vlákno.

Popište kroky navázání komunikace pomocí WebSocketu.

> 1. Handshake - Upgrade protokolu (`Connection: Upgrade` HTTP hlavička)
> 2. Přenost dat

Co je to CAP theorem?

> - Consistency (all nodes see the same data at the same time)
> - Availability (a guarantee that every request receives a response about whether it succeeded or failed)
> - Partition Tolerance (the system continues to operate despite arbitrary message loss or failure of part of the system)

Co je to XSS? Jak funguje?

> Cross Site Scripting - vložení útočného JS na stránku třetí strany

Uveďte 3 různé možnosti jak streamovat data na webu.

> - polling
> - long-polling
> - streaming
>   - open-connections
>   - chunked
>   - server-sent events (uses open or chunked)

Co jsou to "typed literals"? Kde se využívají?

> V RDF, explicitně říká, že hodnota je nějaký datový typ.

Kdy je vhodné použít JSON a kdy XML?

> Při práci s JS je JSON nativní, JSON když nepotřebuju validaci nebo je po deserializaci snadná. JSON je většinou méně objemnější, než XML.

Jaký je rozdíl mezi webovou službou a webovým API?

> API je rozhraní, webová služba je služba.

Co jsou to Linked-Data?

> Použití webu ke spojení dat, která dosud nebyla spojena.

Co je URI reference?

Co je to DOM?

Uveďte 3 typy mashupu.

Co je to RDFa, k čemu je dobrý a uveďte příklad.

Co je to CORS?

Jaký je rozdíl mezi Copyright a Creative Commons?

Jakou Web 2.0 vlastnost bychom přiřadili Wikipedii?

Co definuje specifikace AtomPub?

Jsou HTTP metodu bychom použili pro zhasnutí světla v místnosti?

Jaký je rozdíl mezi XSS a CSRF?

Rozhodněte o správnosti tvrzení a zdůvodněte vaši odpověď: "Jazyk RDF definuje třídy a vztahy mezi nimi."

Uveďte jiné technologie pro streamování dat na webu, kromě WebSocket.

Vyjmenujte metody pro real-time web.

Co je to Slug?

Jak je reprezentován stav aplikace v HATEOAS?

K čemu slouží HTTP metoda PATCH?

Co je to Conditional PUT? Kdy a kde se používá?

Co je predicate v RDF?

Jaký je rozdíl mezi pollingem a long-pollingem?

Vysvětlete co znamená COMET a jakými dvěma způsoby je možné COMET realizovat.

Rozdíl mezi technikami pro streaming SSE a WebSocket.

K čemu slouží autorizační endpoint a tzv. token endpoint u OAuth2? Jaký je mezi nimi rozdíl?

Jaký je rozdíl mezi proprietárním XML, AtomFeed a RDF?

Pomocí čeho lze v RDF vytvořit entitu bez globálního identifikátoru?

Co je to "closure" v Javascriptu?

Co je to Server-Sent Events? Popište na straně serveru i klienta.

Jaký je rozdíl OpenID a OAuth protokoly?

Jak se nazývá hlavní bezpečnostní omezení AJAX aplikace při vývoji mashup aplikace na straně klienta a jak je možné toto omezení "obejít"?

Vysvětlete termín "Multitenancy".

Vysvětlete souvislosti mezi mikroslužbou, kontejnerizací a multi-tenancy.

Uveďte tři technologie kernelu linuxu, které se využívají pro kontejnerizaci.

Jaký je rozdíl mezi WebSocket s XHR?

K čemu je TURN pro WebRTC komunikaci?

Co je to JSON Web Token a k čemu se používá? Uveďte příklad toho, co se v hlavičce objeví.

Co jsou to omezení REST a uveďte příklad jednoho omezení.

> Tato omezení aplikovaná na architekturu jsou základem RESTful služeb. Mezi omezení patří:
> - Uniform interface
> - Stateless
> - Cacheable
> - Client-Server
> - +2

Jaký je rozdíl mezi mikroslužbou a REST službou?

Jaký je rozdíl mezi autorizací a autentizací?

Uveďte rozdíl mezi strong a weak ETagem.

Vysvětlete rozdíl mezi URI a URL.

Vysvětlete SWARM v Dockeru.

Co je to OAuth2 a v čem spočívá přínos OAuth2 pro Web 2.0?

Vysvětlete rozdíl mezi IaaS a PaaS.

K čemu slouží STUN?

> STUN server slouží k detekci nastavení a povaze NAT, aby klienti za NAT mohli uskutečnit mezi sebou spojení.

Co jsou to data volumes v technologii Docker?

> Oddělené adresáře (mimo union-fs) containerů, určené k zápisu a čtení.

Jak autentizovat uživatele použitím JSONP?

> No moc dobře ne. ^[https://www.google.com/search?client=safari&rls=en&q=jsonp+authentication&ie=UTF-8&oe=UTF-8] 

Popište jak funguje CORS.

> Server definuje pomocí HTTP hlavičky `Access-Control-Allow-Origin` domény, ze kterých se mohou tyto requesty provádět. Prohlížeče uvedou HTTP hlavičku `Origin` s názvem domény, ze které request pochází.

Co definuje Atom Syndication Format?

> XML formát pro Atom feedy

K čemu v HTTP Digest slouží hodnota "nonce"?

> Nonce je hodnota, která se používá jen jednou. Odesílá server a slouží klientovi k výpočtu odpovědi.

Co znamená, že Microformát neškáluje?

> Má omezený slovník, nelze jej rozšířit a nemá namespaces.

K čemu slouží iniciativa schema.org?

> Jde o implicitní standard při definici slovníku microdata

Co znamená Representational State Transfer?

> REST - architektura rozhraní (architecture style)

Co je to uniform interface, uveďte jeden příklad a jaké operace zahrnuje?

> Jedno z REST omezení, omezující počet operací na konečný daný počet (CRUD).

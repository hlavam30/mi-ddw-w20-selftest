---
geometry: margin=0.66in
fontsize: 9pt
header-includes:
    - \setlength{\parskip}{0.1in}
---

# MI-DDW 2017/18 Self-test sheet na teorii — výsledky

*Self-test sheet pro MI-DDW, obsahující otázky a odpovědi z fit-wiki zkoušek z LS 2016/2017, kromě duplikátů, plus obsahuje pár otázek navíc, které jsou z jiných zdrojů. Některé odpovědi jsou přímo z fit-wiki, některé jsou přeformulované, aby byly přesnější, nebo srozumitelnější. Doporučený postup: Vytisknout si self-sheet a udělat si test "nanečisto" a napsané výsledky ověřit oproti řešení níže.*

**Co je text mining a popište jednotlivé kroky**

> Extrakce zajímavé (a potenciálně užitečné) informace z textu.
> 
> Postup:
> 1. Sestavení korpusu (prostě velký hromady textu co budu procesovat).
> 1. Normalizace textu, vyčistění od bordelu. Případně: Stemming, Lematization.
> 1. Feature extraction.
> 1. Feature selection. Redukuje dimenzi.
> 1. Hledání patternů, vlastní data mining.

**Co je disambiguation a uvést příklad**

> Ambiguous znamená "nejednoznačnost". Při rozpoznávání věty představuje neurčitost a je považována spíše jako neužitečná informace. Disambiguation je proces odstranění nejednoznačnosti.
> Příklad nejednoznačnosti: slovo "player" (football player, cd player, etc...)

**Popište Coreference Resolution.**

> Různá slova v textu mohou mít stejný význam, tímhle se snažím takový slova sdružit. Př: Mary Smith a Mrs. Smith jsou jedna osoba

**Jaký je rozdíl mezi normálním a invertováným indexem?**

> Normální index udržuje u dokumentů odkazy na termy, které se v dokumentu vysktují, invertovaný index udržuje u termů odkazy na dokumenty, ve kterých se termy vyskytují.

**Jak lze zkomprimovat index? Uveďte 3 metody.**

> - Konkatenací termů do dlouhého řetězce + pointery na termy (`foodfootballfacebook`)
> - Konkatenací termů do dlouhého řetězce s uvedením délky, bez pointerů (`4food8football8facebook`)
> - Front Coding (`8automat*a3ion`)

**Jak zajistí Google matice v kontextu pagerank neuváznutí?**

> Pomocí damping faktoru - přidání malé pravděpodobnosti náhodného skoku na libovolnou stránku.

**Uveďte co je a k čemu slouží tzv. „Reservoir Sampling“.**

> Způsob udržování zpracovávaných dat při dolování streamů. Mám buffer zpracovávaných vzorků, když přijde nový vzorek, tak ho s určitou pravděpodobností uložím do bufferu a nějaký (náhodný) jiný vyhodím.

**Jak řešit při crawlování URI odkazující na stejný zdroj?**

> Normalizací URL a následné smazání duplicit, smazání již navštívených stránek, či přepočítání stejných URL na prioritu.

**Co jsou Motifs a kde se využívají?**

> Lokální vlastnosti grafů. Definované jako rekurentní a statisticky signifikantní podgrafy nebo vzory. Používají se u komplexních sítí, kde je potřeba najít nějaký vzor ve struktuře pro její popis.

**Popište, jaké jsou výhody a nevýhody použití doporučovacích systémů.**

> TODO:

**Popište metriky doporučovacích systémů.**

> Pro ohodnocení:
> - RMSE - Root Mean Squared Error
> - Precision, Recall, F-measure
> - NCDG - Normalized Cumulative Discounted Gain
> - CTR - Click through rate
> - Coverage: Novelty, Serendipity, Diversity

**Co je to Deep web a proč je problém pro crawlery?**

> Stránky ukryté za login formem, paywallem, CAPTCHA. Pro crawler jsou tyto stránky nepřístupné a nemůže je zpracovat.

**Vysvětlete k čemu slouží metoda Girvan-Newman a popište základní kroky.**

> Algoritmus pro detekci komunit v grafu.
>
> 1. Výpočet betweenness pro všechny hrany
> 1. Odstranění hrany s největší betweenness
> 1. Opětovný výpočet betweenness pro hrany, které byly postižené odebráním
> 1. Kroky 2 a 3 se opakují, dokud existuje nějaká hrana

**Vysvětlete pojem „neredukovatelný“ (irreducible) v kontextu algoritmu PageRank. Jak je to řešeno v Google matici?**

> Matice je ireducibilní, pokud se lze z každého uzlu (stavu) dostat do každého jiného uzlu (stavu) (silně souvislá). Matice S při počítání často ireducibilní není. Řeší se damping faktoru.

**Co je to "reciprocity"?**

> Poměr obousměrných hran k celkovému počtu propojení uzlů.

**Co je clique percolation?**

> Metoda detekce překryvu komunit.

**Co je NER a popište postup.**

> Named-entity recognition
> 
> 1. Rozpoznání (rozpoznání textových fragmentů se zmínkami o entitě)
> 2. Klasifikace (přidání tříd ke zmínce o entitě)
> 3. Entities Disambiguation / Entity Linking

**Stemming, Lemmatization - popsat a příklad.**

> Stemming je process extrahování kořene slova. Lemmatization je převedení slova do své kanonické formy.

**Vysvětlete, co je to normalizace URL a kde se využívá. Dále uveďte minimálně dva příklady.**

> Transformace URL do kanonické formy. Využívá se při crawlování stránek, kdy získáváme url dalších stránek, které chceme navštívit. Převodem do kanonické formy zajistíme, že danou adresu navštívíme pouze jednou. Příklady: lower-casing, relative URL -> absolute URL, path resolving

**Vysvětlete, jakým způsobem se může identifikovat robot (crawler). Uveďte příklad, jaký má identifikace význam?**

> Pomocí HTTP hlavičky User-Agent, význam je především etický.

**Popište Lossy Counting v dolování streamů.**

> stream rozdělím na chunky, jeden chunk zpracuju kompletně, po jeho konci od všeho odečtu 1. Tímhle identifikuju časté itemy

**Uveďte jak vypadá výpočet TF-IDF a popište jej.**

> TF - term frequency  
> IDF - inverse document frequency
> 
> $$TF = \frac{\text{počet výskytů slova A}}{\text{počet všech slov v dokumentu}}$$
> $$IDF = \frac{\text{počet dokumentů, kde se A vyskytuje}}{\text{počet všech dokumentů}}$$ 
> $$TFIDF = TF \times IDF$$

**Zadefinujte jednou větou následující pojmy: Precision, Recall, Accuracy, F-measure**

> - Precision (přesnost) ‒ Jak velkou část relevatních záznamů mám ve výsledku. Definice: P(relevant|retrieved)
> - Recall (úplnost) ‒ Jak moc výsledků jse mnašel. Definice: P(retrieved|relevant)
> - Accuracy ‒ Podíl mezi správně klasifikovanými (tp, tn) a všemi záznamy (tp + tn + fp + fn).
> - F-measure ‒ $$F1 = 2 \cdot \frac{\text{recall}\times\text{precision}}{\text{recall} + \text{precision}}$$

**Co je inverted index, výhody a nevýhody?**

> Invertovaný index udržuje u termů odkazy na dokumenty, ve kterých se termy vyskytují.
> 
> TODO:

**Popište, co je to NIF.**

> NLP Interchange Format - Aims to achieve interoperability between NLP tools and language resources

**Co je to Bounce rate?**

> Procento návštěvníků webu, kteří ho opustí po shlédnutí jedné stránky. Někde to nevadí, třeba blogy.

**Popište postup WUM.**

> - *Preprocessing, data collection* - dostanu logy, musím to vyčistit, identifikovat transakce, doplnit informace o session na základě znalosti struktury stránky atd.
> - *Pattern discovery* - detekce patternů pomocí machine learningu, statistických toolů; sumarizace sessions, uživatelů
> - *Pattern analysis* - filtrování, agregace, validace, interpretace nalezených vzorů chování

**Uveďte 3 klady a zápory Collaborative Filltering.**

> \+ lidé získávají relevantní výsledky -> větší zisk  
> \+ škálovatelné díky automatizaci  
> \- netransparentní  
> \- je nutné vlastnit nějaká data  
> \- napadnutelné  

**Co jsou to sitemaps a jakou mají strukturu?**

> Speciální soubor popisující strukturu daného webu. Obsahuje metadata k jednotlivým stránkám, jako je frekvence aktualizace, či jejich priorita.

**Popište hub a autoritu v HITS.**

> Autorita - stránka s hodně inlinkama (důvěryhodný web), Hub - stránka s hodně outlinka (rejstřík, directory)

**Vysvětlete fenomén malého světa („small world phenomenon“) a uveďte libovolný příklad.**

> Tvrzení, že cesta mezi dvěmi libovolnými lidmi je krátká. Např. dle „Six degrees of separation“ je každý člověk schopen se dostat ke komukoliv jinému přes maximálně 6 dalších lidí (kroků).

**Uveďte základní rozdíl mezi implicitními a explicitními daty používanými při WUM.**

> Explicitní: získaná přímo např. z dotazníků, hodnocení, Implicitní: automatické sledování uživatelů - kam klikají, co dělají, eye tracking

**Který z následujících přístupů kolaborativního filtrování bývá výpočetně méně náročný - „user-based“ nebo „item-based“? Svou odpověď zdůvodněte.**

> Item-based, itemů je typicky méně, než uživatelů.

**Uveďte 2+ Attacks on collaborative filtering.**

> Random attack, Average attack, Bandwagon attack, Segment attack, Love/Hate attack, Reverse Bandwagon, Probe attack

**Co jsou sketches v data streams?**

> TODO: 

**Chceme ukládat a indexovat dokumenty, jak to uděláme, aby v tom šlo efektivně vyhledávat? Uveďte výhody a nevýhody.**

> - Hashmapou - Hledání v ní je O(1). Na menší data dobrý. Nedá se hledat prefixově. Nutnost rehashe občas, což je drahá operace.
> - Stromem (B-Strom) - Obecně u stromů je drahý rebalancování. B-Stromy to řeší. Vyhledávání O(log N). Jde v tom dělat prefixový vyhledávání.

**Jak porovnávat indexy a query při vyhledávání?**

> TODO: 

**Co je to hashování indexu? Uvěďte 3 výhody či nevýhody.**

> + rychlost

**Jak lze rozpoznat uživatele?**

> Cookies, User-Agent, browser fingerprinting, IP adresa, kombinace

**Popište, co je to Parts of Speech a uveďte příklad.**

> Slovní druh (NN - noun, PN - proper noun, JJ - adjective, ...)

**Co to je konverzní trychtýř?**

> Trend snižujícího se počtu lidí provádějící po sobě následující akce, dokud nedojde k nákupu cíleného produktu.  
> Např.: 10000 lidí vidí reklamu, 700 z nich na ni klikne, 400 z nich otevře detail produktu, 60 z nich hodí produkt do košíku atd.

**Co jsou HITS a jak se počítají?**

> HITS - Hypertext Induced Topic Search  
> Podobný pageranku, query dependent, pro každou query se počítá znova
> 
> Postup:
> 1. vezme se result = set stránek,
> 1. expanduje se tenhle result o stránky na které původní set odkazuje
> 1. vyprodukuje se orankování tohodle expandovaného subsetu

**Co to jsou Asociační pravidla a ukažte jejich příklad. Uveďte 2 metriky.**

> Asociační pravidlo: „Když si koupí párek a hořčici tak si taky často koupí pivo.“ Používá dvě metriky:
> *Confidence:* Poměr: $$\frac{\text{nakupy co obsahjou: pivo + parek + horcice}}{\text{nakupy co obsahjou: pivo + parek}}$$
> *Support:* procento nákupů obsahuje párek, hořčici i pivo: $$\frac{\text{nákupy které obsahujou pivo + parek + horcici}}{\text{všechny nákupy}}$$

**Popište pseudokódem, či slovně Apriori algoritmus.**

> 1. Definuju si minimální confidnce a support
> 1. Z transakcí co mám začnu genrovat množiny, nejprve jednoprvkové pak dvouprvkové, tříprvkove, atd...
> 1. Pro každou tu množinu spočtu support v transakcích
> 1. Vyhážu ty co mají support < minimum
> 1. Když mám ty množiny začnu z nich dělat permutace
> 1. Vyberu jen ty co mají permutaci > min.

**Popište podrobně Kernighan-Lin.**

> Algoritmus na graph partitioning.

**Uveďte metriky pro porovnání důležitosti uzlu.**

> - Degree centrality: (in-) or (out-) degree is the number of edges that lead into or out of the node.
> - Closeness centrality: The mean length of all shortest paths from a node to all other nodes.
> - Betweeness centrality: The number of shortest paths that pass through a node divided by all shortest paths.
> - Eigenvector centrality: A node with high eigenvector centrality is connected to other nodes with high eigenvector centrality.

**Popište Bow-tie.**

> Model struktury internetu.  
> Skládá se z centrálního jádra, IN a OUT stránek, tendrils, tubes a disconnected uzlů.

**Definujte termíny "seed page", "frontier" a "fetcher" z terminologie crawlingu.**

> - Seed pages - seznam počátečních URL, ze kterých se pak jede dál
> - Frontier - seznam URL, které čekají na prozkoumání, tj. jakási fronta
> - Fetcher - řeší stahování stránky

**Uveďte dvě strategie crawlování.**

> BFS nebo Backlink

**Definujte homofilii (homophily).**

> Mám dva typy uzlů, např. muži-ženy, bílý-černý. Homophily je, pokud se spolu druží spíš v rámci pohlaví/rasy než aby se družili se všema.

**Uveďte 3 typy počítání frekvencí při data streams. Jeden z nich popište.**

> - Count-min Sketch - ukládám počty v omezeném poli, indexuju podle hash funkce - ztrácím informaci, počitadlo udává spodní mez (může jich být nakombinováno víc, kvůli kolizi hash fce)
> - Lossy Counting - stream rozdělím na chunky, jeden chunk zpracuju kompletně, po jeho konci od všeho odečtu 1. Tímhle identifikuju časté itemy.
> - Distinct items counting - chci vědět, kolik rozdílných itemů přišlo. Efektivně HyperLogLog algoritmus: itemy zahashuju do binární podoby, najdu nejvyšší řetězec trailing nul a odhadnu, že počet unikátních itemů bude $$2^n$$.
>   - Kdybysme viděli 2 různé itemy, čekal bych 1 trailing nulu.
>   - Kdybysme viděli 4 různé itemy, čekal bych 2 trailing nuly, atd.

**Jak jinak lze roboty žádat o změny v chování, kromě souboru `robots.txt`?**

> meta tagy v HTML hlavičce stránky, X-Robots-Tag headerem v HTTP requestu

**Co to je crawler/spider past?**

> Dynamicky generovaná smyčka linků. Robot v ní uvázne.

**Co je Structure Hole?**

> Přístup k informacím z částí grafu, které spolu neinteragují. Takový uzel se stává "interfacem" mezi dvěmi částmi grafu.

**Popište rozdíl mezi Information Retrieval a Recommending System.**

> IR - vím přesně co chci (vyhledávám), RS - nevím co chci (nechám si doporučit)

**Jak lze predikovat další hranu v grafu sítě? Uveďte jeden způsob.**

> - Graph distance
> - Common Neighbors

**Uveďte tři vstupní data WUM.**

> - Usage data - data z produkce o přístupech (logy)
> - Content data - kolekce objektů a vztahů mezi nimi
> - Structure data - organizace obsahu na portálu

**Co je to tokenizace v kontextu text miningu?**

> Seskupování znaků do logických celků.

**Popište výpočet Edge Betweeness.**

> TODO:

**Co je Preferential Attachment?**

> Většina nových hran vede do uzlů s již vysokým stupněm.

**Co je to opinion mining? Uveďte dva typy.**

> Identifikace názorů, emocí, sentimentu z textu - většinou se zkoumá pozitivní, negativní nebo neutrální  
> Lexicon-based, sentiment learning

**Definujte clustering coefficient a bridge.**

> Pravděpodobnost, že dva náhodní kamarádi daného uzlu jsou taky navzájem kamarádi. Udává koncentrovanost vztahů v clusteru, či globálně úroveň clusterovatelnosti.

**Co jsou to matice UPM (user-pageview matrix), PFM (pageview-feature matrix) a TFM (transaction-feature matrix)?**

> UPM - matice mající v řádcích transakce pro každou webovou stránku, PFM - matice přiřazující váhami jisté features každé stránce a TFM je vynásobení matic UPM a PFM mající v řádcích transakce pro každou určenou feature.

**Jak lze testovat kvalitu Recommender systems?**

> - offline (simulace uživatelů)
> - user-studies
> - online (a/b testing, hrozí nespokojenost uživatelů)

**Čím snížíme dimenzionalitu textu při text miningu?**

> lemmatizací, stemmingem, odstranění stopwords a převedení na synonyma a antonyma

**Jak se lze bránit útokům na CF?**

> unsupervised, supervised, podle chování uživatele, srovnání hodnocení proti ostatním, CAPTCHA.

**Uveďte 3 typy samplingu data streams.**

> - Reservoir sampling - udržuju si buffer vzorků, když přijde novej, s určitou pravděpodobností ho uložím a nějakej jinej vyhodim.
> - Moving windows - okénko, udržuju si jen omezenou historii. Tímhle řeším to, že třeba stará data expirovala.
> - Sketching - nesoustředím se jen na poslední kus dat, snažím se sumarizovat všechna data, s různou úrovní detailů

**Uveďte dva modely Information Retrieval.**

> Boolean model a Vector Space model